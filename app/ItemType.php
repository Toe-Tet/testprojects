<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Category;

class ItemType extends Model
{
    protected $fillable = [
        'name', 'keyword', 'categoryid', 'categoryitemtypeid'
    ];

    public function categories()
    {
        return $this->belongsTo('App\Category');
        // return Category::all();
    }

    public function getCategories()
    {
        $category = $this->category_id;
        if($category instanceof Category)
        {
            return [$category->getName()];
        }
    }

    // public function categoryitemtypes()
    // {
    //     return $this->belongsTo(CategoryItemType::class);
    // }
}

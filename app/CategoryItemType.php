<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryItemType extends Model
{
    protected $fillable = [
        'name'
    ];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.categories.index');
    }

    public function categoryData()
    {
        return Datatables::of(Category::query())
        ->addColumn('action', function($category){
            return view('admin.categories.action', compact('category'))->render();
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('withgender'));
        $this->validate($request, [
            'name' => 'required|string',
            'keyword' => 'required|string',
        ]);

        Category::create([
            'name' => $request->input('name'),
            'keyword' => $request->input('keyword'),
        ]);
        
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('id', '=', $id)->get();
        return view('admin.categories.edit', compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'keyword' => 'required|string',
        ]);

        $category = Category::find($id);
        $category->update($request->all());
        
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        // $cover = Book::where('idx', '=', $id)->select('cover_photo')->get();
        // foreach ($cover as $photo) {
        //     $photo = $photo->cover_photo;
        // }
        // $coverphoto = '/photo/'.$photo;
        // $cover = Table::select('cover_photo')->where('idx', $id)->get();
        // unlink($_SERVER['DOCUMENT_ROOT'].$coverphoto);
        // Storage::delete($coverphoto);
        // DB::table('books')->where('idx', '=', $id)->delete();
        return back();
    }
}

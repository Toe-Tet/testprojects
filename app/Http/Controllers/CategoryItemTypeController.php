<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryItemType;
use DataTables;

class CategoryItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categoryitemtypes.index');
    }

    public function categoryitemtypeData()
    {
        return Datatables::of(CategoryItemType::query())
        ->addColumn('action', function($categoryitemtype){
            return view('admin.categoryitemtypes.action', compact('categoryitemtype'))->render();
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categoryitemtypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('withgender'));
        $this->validate($request, [
            'name' => 'required|string',
        ]);

        CategoryItemType::create([
            'name' => $request->input('name'),
        ]);
        
        return redirect()->route('categoryitemtypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryitemtypes = CategoryItemType::where('id', '=', $id)->get();
        return view('admin.categoryitemtypes.edit', compact('categoryitemtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
        ]);

        $categoryitemtype = CategoryItemType::find($id);
        $categoryitemtype->update($request->all());
        
        return redirect()->route('categoryitemtypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryitemtype = CategoryItemType::find($id);
        $categoryitemtype->delete();

        return back();
    }
}

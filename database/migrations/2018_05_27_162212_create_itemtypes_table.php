<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('keyword', 50);
            $table->unsignedInteger('categoryid');
            $table->unsignedInteger('categoryitemtypeid');
            $table->timestamps();

            $table->foreign('categoryid')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('categoryitemtypeid')->references('id')->on('category_item_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemtypes');
    }
}

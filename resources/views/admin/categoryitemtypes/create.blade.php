
@extends('admin.layouts.master')

@section('title')

	<title>Category Item Types Register</title>

@endsection

@section('content')

	<div class="container">

		<h3>Category Item Type Registration</h3>
		<form action="{{ route('categoryitemtypes.store') }}" method="POST">
			@csrf

			<div class="container" style="margin-top: 20px;">
				<label for="name" class="form-label">
					Category Item Type Name
				</label>
				<input type="text" class="form-control" name="name">
			</div>

			<div class="container" style="margin-top: 20px;">
				<input type="submit" class="btn btn-primary" value="Register">
			</div>


			

		</form>
	</div>
@endsection

@extends('admin.layouts.master')

@section('title')

	<title>Item Type Register</title>

@endsection

@section('content')

	<div class="container">

		<h3>Item Type Registration</h3>
		<form action="{{ route('itemtypes.store') }}" method="POST">
			@csrf

			<div class="container" style="margin-top: 20px;">
				<label for="name" class="form-label">
					Item Type Name
				</label>
				<input type="text" class="form-control" name="name" required>
			</div>

			<div class="container" style="margin-top: 20px;">
				<label for="keyword" class="form-label">
					Keyword
				</label>
				<input type="text" class="form-control" name="keyword" required>
			</div>

			<div class="container" style="margin-top: 20px;">
				<label for="category" class="form-label">
					Category
				</label>
				<select name="category" class="form-control" id="" required>
					<option value="" disabled selected>Choose Category</option>
					@foreach($categories as $category)
						<option value="{{ $category->id }}">{{ $category->name }}</option>
					@endforeach
				</select>
			</div>

			<div class="container" style="margin-top: 20px;">
				<label for="categoryitemtype" class="form-label">
					Category Item Type
				</label>
				<select name="categoryitemtype" class="form-control" id="">
					<option value=""  disabled selected>Choose Item Type</option>
					<option value="0">None</option>
					@foreach($categoryitemtypes as $categoryitemtype)
						<option value="{{ $categoryitemtype->id }}">{{ $categoryitemtype->name }}</option>
					@endforeach
				</select>
			</div>

			<div class="container" style="margin-top: 20px;">
				<input type="submit" class="btn btn-primary" value="Register">
			</div>


			

		</form>
	</div>
@endsection
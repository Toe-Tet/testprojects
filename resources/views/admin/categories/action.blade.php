<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary"> Edit </a>

<form action="{{ route('categories.destroy', $category->id) }}" method="POST" style="display: inline;" id="delete-form">
	{{ csrf_field() }}
	{{ method_field('DELETE') }}

	<button type="submit" class="btn btn-danger" id="delete-btn"> Delete </button>
</form>
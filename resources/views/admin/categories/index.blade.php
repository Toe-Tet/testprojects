@extends('admin.layouts.master')

@section('title')
<title>Categories - Admin Panel</title>
@endsection

@section('content')
<div class="row">
	<div class="col-md-10">
	</div>
	<div class="col-md-2">
		<div class="container">
			<a href="{{ route('categories.create') }}" class="btn btn-primary text-right">
				Register
			</a>
		</div>	
	</div>
</div>

<hr>
<!-- /.row -->
<div class="container">
    <h3>Categories List</h3>
    <table class="table table-bordered" id="categories-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Category Name</th>
                <th>Keyword</th>
                <th>Datetime</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('script')

<script>

$(function() {
    $('#categories-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('category.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'keyword', name: 'keyword' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});

</script>

@endpush


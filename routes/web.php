<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Categories
Route::resource('categories', 'CategoryController');
Route::get('category/data', 'CategoryController@categoryData')->name('category.data');

// Category Item Types
Route::resource('categoryitemtypes', 'CategoryItemTypeController');
Route::get('categoryitemtype/data', 'CategoryItemTypeController@categoryitemtypeData')->name('categoryitemtype.data');

// Item Types
Route::get('itemtype/data', 'ItemTypeController@itemtypeData')->name('itemtype.data');
Route::get('something','ItemTypeController@test');
Route::resource('itemtypes', 'ItemTypeController');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
